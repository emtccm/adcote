subroutine geometry_writer(atom,coord,dmax,ffmt,fout,uout)
    implicit none

    character(len=2) :: atom
    character(len=*) :: ffmt,fout

    integer :: dmax,i,uout

    real(kind=8), dimension(:,:) :: coord(3,dmax)

    open(unit = 16, file = fout, action = 'write')

    write(unit = 16, fmt = *) dmax
    write(unit = 16, fmt = *) ''
    do i = 1, dmax, 1
        write(unit = 16, fmt = ffmt) atom, coord(:,i)
    end do

    close(unit = 16) 

end subroutine geometry_writer
