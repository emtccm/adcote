subroutine final_results(finish,fout,geom_save,hist_save,prt_save,radial_save,start,uout,Vmean,Vstd,Vvar)
    implicit none

    character(len=25) :: fout

    integer :: uout

    logical :: geom_save,hist_save,prt_save,radial_save

    real(kind=8) :: finish,start,Vmean,Vstd,Vvar

    write(unit = uout, fmt = '(1x,a19,1x,a18,1x,a19)') '*******************','SIMULATION RESULTS','*******************'
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a60)') '============================================================'

    if (hist_save .eqv. .true.) then
    write(unit = uout, fmt = '(15x,a1,4x,a6,4x,a1,3x,a9,2x,a1,3x,a8,3x,a1)') '|', 'Energy', '|', 'Std. dev.', '|', 'Variance', '|'
        write(unit = uout, fmt = '(1x,a60)') '------------------------------------------------------------'
    write(unit = uout, fmt = '(1x,a,1x,3(a,1x,f12.6,1x),a)') 'Final results', '|', Vmean, '|', Vstd, '|', Vvar, '|'
     write(unit = uout, fmt = '(1x,a60)') '------------------------------------------------------------'
    write(unit = uout, fmt = '(1x,a60)') '------------------------------------------------------------'
    end if

    if (prt_save .eqv. .true.) then
        write(unit = uout, fmt = '(1x,a,1x,a)') 'Potential Energy save file :', 'potential.out'
    end if

    if (hist_save .eqv. .true.) then
        write(unit = uout, fmt = '(1x,a,1x,a)') 'Potential Energy Histogram save file :', 'potential_histogram.out'
    end if

    if (radial_save .eqv. .true.) then
        write(unit = uout, fmt = '(1x,a,1x,a)') 'Radial Distribution Function save file :', 'rad_dist_func.out'
    end if

    if (geom_save .eqv. .true.) then
        write(unit = uout, fmt = '(1x,a,1x,a)') 'Final Geometry save file :', trim(fout)
    end if

    write(unit = uout, fmt = '(1x,a19,g16.8,a3)') 'Time constumption :', finish-start, '(s)'
    write(unit = uout, fmt = '(1x,a60)') '============================================================'
    write(unit = uout, fmt = *) ''

end subroutine final_results
