subroutine hist_results(hist_delta,hist_frag,hist_save,Vmin,Vpop,uout)
    implicit none

    integer :: hist_frag,hmax,i,uout
    integer, dimension(:) :: Vpop(hist_frag)

    logical :: hist_save

    real(kind=8) :: hist_delta,Vmin

    hmax=maxval(Vpop(:))

    write(unit = uout, fmt = '(1x,a14,1x,a,1x,a14)') '**************','POTENTIAL ENERGY HISTOGRAM','**************'
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a16,2x,a10)')  'Potential Energy', 'Population'
    write(unit = uout, fmt = '(1x,a28)') '---------------------------'
    do i = 1, hist_frag, 1
        write(unit = uout, fmt = '(1x,g16.8,3x,f16.8)') i*hist_delta+Vmin, real(Vpop(i))/real(hmax)
    end do
    write(unit = uout, fmt = '(1x,a28)') '----------------------------'
    write(unit = uout, fmt = *) ''

    if (hist_save .eqv. .true.) then
        open(unit = 15, file = 'potential_histogram.out', action = 'write') 
        do i = 1, hist_frag, 1
            write(unit = 15, fmt = '(1x,g16.8,3x,f16.8)') i*hist_delta+Vmin, real(Vpop(i))/real(hmax)
        end do
        close(unit = 15) 
    end if

end subroutine hist_results
