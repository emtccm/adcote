function cutoff(rc,tx,ty,tz,uni_xyz,vc) result(v)
    implicit none

    real(kind=8) :: r,rc,r1,r2,tx,ty,tz,vc,v
    real(kind=8),dimension(:) :: uni_xyz(3)

    r=(tx-uni_xyz(1))**2
    r=r+(ty-uni_xyz(2))**2
    r=r+(tz-uni_xyz(3))**2

    if (r .lt. rc**2) then
        r1=r*r*r
        r2=r1*r1
        v=4*(1/r2-1/r1)-vc
    else
        v=0.d0
    end if

end function cutoff
