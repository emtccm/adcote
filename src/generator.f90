program generator
    use omp_lib
    implicit none

    character(len=2) :: atom
    character(len=50) :: ffmt,fout
    character(len=80) :: comment

    integer :: hist_frag,hist_next,hist_step,ind,o

    integer :: equiv,frag,i,iter,j,k,m,max_threads,n,Nmax,Nsteps,prt_next
    integer :: prt_step,radial_next,radial_step,thread_num,uout,z,zmax
    integer, dimension(:), allocatable :: mc_rand,pop,Vpop

    logical :: hist_save

    logical :: geom_save,prt_save,radial_save

    real(kind=8) :: dh,Vmax,Vmin
    real(kind=8), dimension(:), allocatable :: Vhist

    real(kind=8) :: aux,cutoff,dr,dv,fin,finish,init,L,r1,r2,radial_max
    real(kind=8) :: radial_min,rc,rmin,start,t,tx,ty,tz,v,v0,v1,vc,vconv,vmean
    real(kind=8) :: vstd,vvar
    real(kind=8), dimension(:) :: new_xyz(3),tra_xyz(3)
    real(kind=8), dimension(:), allocatable :: rad_dist,v_i,Vdata
    real(kind=8), dimension(:,:), allocatable :: uni_xyz,v_ij

!=====
!   INPUT VARIABLES
!=====

    open(unit = 15, file = 'INPUT', action = 'read') 

    read(unit = 15, fmt = *) fout           ! Ouput file

    read(unit = 15, fmt = *) L              ! Length of the cube
    read(unit = 15, fmt = *) Nmax           ! Number of particles
    read(unit = 15, fmt = *) Nsteps         ! Number of steps
    read(unit = 15, fmt = *) T              ! Temperature

    read(unit = 15, fmt = *) atom           ! Atom type
    read(unit = 15, fmt = *) equiv          ! Number of equilibration steps
    read(unit = 15, fmt = *) geom_save      ! Save the final geometry
    read(unit = 15, fmt = *) rc             ! Cutoff radius
    read(unit = 15, fmt = *) thread_num     ! Number of threads for parallelization

    read(unit = 15, fmt = *) zmax           ! Number of trials before reducing rmin
    read(unit = 15, fmt = *) rmin           ! Minimum interatomic distance for nintial trials

    read(unit = 15, fmt = *) prt_step       ! Periodicity of recording potential energy
    read(unit = 15, fmt = *) prt_save       ! Save potential energy to file

    read(unit = 15, fmt = *) radial_save    ! Save radial distribution function to file
    read(unit = 15, fmt = *) radial_step    ! Periodicity of recording g(r)
    read(unit = 15, fmt = *) dr             ! Wide of radial steps for radial distribution function

    read(unit = 15, fmt = *) hist_save      ! Save potential energy histogram to file
    read(unit = 15, fmt = *) hist_step      ! Periodicity of recording potential energy for the histogram
    read(unit = 15, fmt = *) dh             ! Wide of potential energy for histogram population sum

    read(unit = 15, fmt = '(a)') comment

    close(unit = 15) 

!+++++

!=====
!   INITIAL SETUP
!=====

    init=OMP_get_wtime()

!-----
!   Parallelization Setup

    max_threads=OMP_get_max_threads()
    if (thread_num .ge. max_threads) then
        thread_num=max_threads
    end if
    call OMP_set_num_threads(thread_num)

!+++++

!-----
!   Global Variables

    frag=int((L-rc-dr)/dr)

    r1=rc**2
    r1=r1*r1*r1
    r2=r1*r1
    vc=4*(1/r2-1/r1)

    uout=20

!+++++

!-----
!   Memory and Dimension Allocation

    allocate(uni_xyz(3,Nmax))
    allocate(v_ij(Nmax,Nmax))
    allocate(v_i(Nmax))
    allocate(Vdata(int((Nsteps*0.1+equiv*0.9)/prt_step)))
    allocate(mc_rand(Nmax))
    allocate(pop(frag))
    allocate(rad_dist(frag))

    if (hist_step .ne. 0) then
        allocate(Vhist(int(Nsteps-equiv)/hist_step))
    end if

!+++++

!-----
!   Initial Data Allocation

    V=0.d0
    pop=0.d0
    v_ij=0.d0

!+++++

    open(unit = uout, file = fout, action = 'write', access='append')
    
    write(unit = uout, fmt = '(1x,a14,1x,a15,1x,a14)') '**************','INPUT DATA FILE','**************'
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a)') trim(comment)
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a,1x,i5)') 'Number of particles :', Nmax
    write(unit = uout, fmt = '(1x,a,1x,f8.2)') 'Temperature :', T
    write(unit = uout, fmt = '(1x,a,1x,f6.2)') 'Cube length :', L
    write(unit = uout, fmt = '(1x,a,1x,i12)') 'Number of steps :', Nsteps
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a,1x,a)') 'Atom type :', trim(atom)
    write(unit = uout, fmt = '(1x,a,1x,i12)') 'Number of equilibration steps :', equiv
    write(unit = uout, fmt = '(1x,a,1x,l1)') 'Save final geometry to file :', geom_save
    write(unit = uout, fmt = '(1x,a,1x,f6.2)') 'Cut off radius :', rc
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a,1x,i12)') 'Initial geometry guess maximum iterations :', zmax
    write(unit = uout, fmt = '(1x,a,1x,f6.2)') 'Minimun initial interatomic distance :', rmin
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a,1x,i5)') 'Periodicity of recording potential energy :', prt_step
    write(unit = uout, fmt = '(1x,a,1x,l1)') 'Save potential energy to file :', prt_save
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a,1x,l1)') 'Save g(r) to file :', radial_save
    write(unit = uout, fmt = '(1x,a,1x,i5)') 'Periodicity of recording g(r) :', radial_step
    write(unit = uout, fmt = '(1x,a,1x,f6.2)') 'g(r) population wide :', dr
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a,1x,l1)') 'Save potential energy histogram to file :', hist_save
    write(unit = uout, fmt = '(1x,a,1x,i5)') 'Periodicity of recording potential energy for histogram :', hist_step
    write(unit = uout, fmt = '(1x,a,1x,f6.2)') 'Potential energy histogram wide :', dh
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a,1x,i3)') 'Number of threads used :', thread_num
    write(unit = uout, fmt = *) ''

!+++++
!+++++

!=====
!   INITIAL STRUCTURE GENERATION AND ANALYSIS
!=====

    start=OMP_get_wtime()

!-----
!   Inital Coordinates Generation and Save

    call initial_geometry(L,Nmax,rmin,uni_xyz,zmax)

!+++++

!-----
!   Initial Potential Calculation

    !$OMP parallel default(firstprivate) shared(V,v_ij)
    !$OMP do reduction(+:V)
    do n = 1, Nmax-1, 1
        do m = n+1, Nmax, 1
            call near_img_conv(L,tx,ty,tz,uni_xyz(:,m),uni_xyz(:,m))

            v_ij(n,m)=cutoff(rc,tx,ty,tz,uni_xyz(:,n),vc)
            v_ij(m,n)=v_ij(n,m)
            V=V+v_ij(n,m)
        end do
    end do
    !$OMP end do
    !$OMP end parallel

!+++++

    finish=OMP_get_wtime()

    write(unit = uout, fmt = '(1x,a14,1x,a22,1x,a14)') '**************','INITIAL STRUCTURE DATA','**************'
    write(unit = uout, fmt = *) ''
    write(unit = uout, fmt = '(1x,a,1x,g16.8)') 'Potential energy :', V
    write(unit = uout, fmt = '(1x,a,1x,g16.8,a)') 'Time consumption :', finish-start, '(s)'
    write(unit = uout, fmt = *) ''

!+++++
!+++++

!=====
!   MONTE CARLO SIMULATION
!=====

    start=OMP_get_wtime()            

!-----
!   Monte Carlo setup

    rad_dist=0.d0
    Vconv=0.d0
    Vmean=0.d0
    k=1
    o=1
    if (hist_step .ne. 0) then
        Vhist=0.d0
    end if

    prt_next=prt_step
    radial_next=radial_step+equiv
    hist_next=hist_step+equiv

    if (prt_save .eqv. .true.) then
        open(unit = 16, file = 'potential.out', action = 'write') 
    end if

!+++++

    write(unit = uout, fmt = '(1x,a14,1x,a22,1x,a14)') '**************','MONTE CARLO SIMULATION','**************'
    write(unit = uout, fmt = *) ''
    write(unit = uout, fmt = '(1x,a5,7x,a6,8x,a12)') 'Cycle', 'Energy', 'Energy diff.'
    write(unit = uout, fmt = '(1x,a38)') '--------------------------------------'

    do j=1, Nsteps, 1

!-----
!   Random Selection of One Atom and Displacement

        mc_rand=(/ (m,m=1,Nmax) /)
        call random_seed
        call random_number(aux)
        n=mc_rand(int(aux*size(mc_rand))+1)
        v_i(:)=v_ij(n,:)
        v0=sum(v_i(:))
        v1=0.d0

        call random_seed
        call random_number(new_xyz)
        do i = 1, 3, 1
            new_xyz(i)=(new_xyz(i)-0.5)*L*L**3/Nmax
        end do

!+++++

!-----
!   Construct Trasformed Geometries Applying Displacement Vector

        new_xyz(:)=uni_xyz(:,n)+new_xyz(:)

        if (new_xyz(1) .lt. 0) then
            new_xyz(1)=new_xyz(1)+L
        else if (new_xyz(1) .gt. L) then
            new_xyz(1)=new_xyz(1)-L
        end if

        if (new_xyz(2) .lt. 0) then
            new_xyz(2)=new_xyz(2)+L
        else if (new_xyz(2) .gt. L) then
            new_xyz(2)=new_xyz(2)-L
        end if

        if (new_xyz(3) .lt. 0) then
            new_xyz(3)=new_xyz(3)+L
        else if (new_xyz(3) .gt. L) then
            new_xyz(3)=new_xyz(3)-L
        end if

!+++++

!-----
!   Calculation of New Potential Energy

        !$OMP parallel default(firstprivate) shared(v_i)
        !$OMP do
        do m = 1, Nmax, 1
            if (m .ne. n) then
                call near_img_conv(L,tx,ty,tz,uni_xyz(:,m),new_xyz(:))

                v_i(m)=cutoff(rc,tx,ty,tz,new_xyz(:),vc)
            end if
        end do
        !$OMP end do
        !$OMP end parallel

!+++++

!-----
!   Evaluation of acceptance of movement

        v1=sum(v_i(:))
        dv=v1-v0
        if (dv .lt. 0) then
            uni_xyz(:,n)=new_xyz(:)
            v_ij(n,:)=v_i(:)
            v_ij(:,n)=v_i(:)
            V=V+dv
        else if (dv .ge. 0) then
            call random_seed
            call random_number(aux)
            if (aux .le. exp(-dv*273/T)) then
                uni_xyz(:,n)=new_xyz(:)
                v_ij(n,:)=v_i(:)
                v_ij(:,n)=v_i(:)
                V=V+dv
            end if
        end if

!+++++

!-----
!   Save data to output and if required to external file potential.out

        if (j .eq. prt_next .and. j .lt. Nsteps) then
            if (j .ne. equiv) then
                write(unit = uout, fmt = '(1x,i5,1x,g16.8,1x,e14.4)') j/prt_step, V, dv
            else
                write(unit = uout, fmt = '(1x,i5,1x,g16.8,1x,e14.4,5x,a)') j/prt_step, V, dv, 'Equilibration steps finished'
            end if
            if (prt_save .eqv. .true.) then
                write(unit = 16, fmt = '(i5,1x,g16.8,1x,e14.4)') j/prt_step, V, dv
            end if
            prt_next=prt_next+prt_step
        end if

!+++++

!-----
!   Radial Distribution Function calculation

        if (j .eq. radial_next .and. radial_step .ne. 0) then
            !$OMP parallel default(firstprivate) shared(pop,rad_dist)
            !$OMP do reduction(+:pop)
            do z = 1, frag, 1
                radial_min=(L-rc)/frag*z
                radial_max=radial_min+dr
                if (rad_dist(z) .lt. dr) then
                    rad_dist(z)=radial_min
                end if
                call radial_distribution(iter,frag,L,Nmax,radial_max,radial_min,rmin,uni_xyz,z)
                pop(z)=pop(z)+iter
            end do
            !$OMP end do
            !$OMP end parallel
            radial_next=radial_next+radial_step
        end if

!+++++

!-----
!   Energy Histogram Calculation

        if (j .eq. hist_next .and. hist_step .ne. 0) then
            Vhist(o)=V
            o=o+1
            hist_next=hist_next+hist_step
        end if

!+++++
    
    end do

!-----
!   Monte Carlo Final Setup

    if (prt_save .eqv. .true.) then
        write(unit = 16, fmt = '(i5,1x,g16.8,1x,e14.4)') j/prt_step, V, dv
    end if

    write(unit = uout, fmt = '(1x,i5,1x,g16.8,1x,e14.4,5x,a)') j/prt_step, V, dv, 'Maximun cycles reached'
    write(unit = uout, fmt = '(1x,a38)') '--------------------------------------'
    write(unit = uout, fmt = *) ''
    write(unit = uout, fmt = *) ''

    if (prt_save .eqv. .true.) then
        close(unit = 16) 
    end if

    finish=OMP_get_wtime()

!+++++
!+++++

!=====
!   FINAL RESULTS
!=====

    write(unit = uout, fmt = '(1x,a60)') '************************************************************'
    write(unit = uout, fmt = '(16x,a31)') 'MONTE CARLO SIMULATION FINISHED'
    write(unit = uout, fmt = '(1x,a60)') '************************************************************'
    write(unit = uout, fmt = *) ''

!-----
!   Radial Distribution Function

    if (radial_step .ne. 0) then
        pop=pop*radial_step/real(Nsteps-equiv)
        call radial_results(dr,frag,L,Nmax,pop,rad_dist,radial_save,uout)        
    end if

!+++++

!-----
!   Enegy Histogram

    if (hist_step .ne. 0) then
        Vmax=maxval(Vhist)
        Vmin=minval(Vhist)
        hist_frag=int((maxval(Vhist)-minval(Vhist))/dh)+1

        allocate(Vpop(hist_frag))
        Vpop=0

        do i = 1, int((Nsteps-equiv)/hist_step), 1
            ind=int((Vhist(i)-Vmin)/dh)+1
            Vpop(ind)=Vpop(ind)+1
        end do

        ind=findloc(Vpop,maxval(Vpop),dim=1)
        Vmean=ind*dh+Vmin
        Vvar=sum((Vhist(:)-Vmean)**2)/((Nsteps-equiv)/hist_frag)
        Vstd=sqrt(Vvar)

        call hist_results(dh,hist_frag,hist_save,Vmin,Vpop,uout)
    end if

!+++++

    if (geom_save .eqv. .true.) then
        ffmt='(a,1x,3(f12.8,1x))'
        fout='final.xyz'
        call geometry_writer(atom,uni_xyz,Nmax,ffmt,fout,uout)
    end if

    call final_results(finish,fout,geom_save,hist_save,prt_save,radial_save,start,uout,Vmean,Vstd,Vvar)
    
!-----
!   Time

    fin=OMP_get_wtime()
    write(unit = uout, fmt = '(1x,a24,1x,g16.8)') 'Total time consumption :', fin-init

!+++++

!+++++
!+++++

    close(unit = uout) 

end program generator
