subroutine initial_geometry(L,Nmax,rmin,uni_xyz,zmax)
    implicit none

    integer :: n,Nmax,m,z,zmax

    real(kind=8) :: L,r,rmin
    real(kind=8), dimension(:,:) :: uni_xyz(3,Nmax)

    n=1
    z=0
    do while (n .lt. Nmax)
        call random_seed()
        call random_number(uni_xyz(:,n))
        uni_xyz(:,n)=uni_xyz(:,n)*L
        if (z .eq. zmax) then
            z=0
            rmin=rmin*0.75
        end if
        do m = 1, n-1, 1
            r=(uni_xyz(1,m)-uni_xyz(1,n))**2
            r=r+(uni_xyz(2,m)-uni_xyz(2,n))**2
            r=r+(uni_xyz(3,m)-uni_xyz(3,n))**2
            if (r .lt. rmin**2) then
                n=n-1
                z=z+1
                exit
            end if
        end do
        n=n+1
    end do

end subroutine initial_geometry
