subroutine radial_distribution(iter,frag,L,Nmax,radial_max,radial_min,rmin,uni_xyz,h)
    use omp_lib
    implicit none

    integer :: iter,frag,h,m,n,Nmax
    integer, dimension(:) :: pop(frag)

    real(kind=8) :: L,r,radial_max,radial_min,rmin,tx,ty,tz
    real(kind=8), dimension(:,:) :: uni_xyz(3,Nmax)

    iter=0.d0
    do n = 1, Nmax-1, 1
        do m = n+1, Nmax, 1
            call near_img_conv(L,tx,ty,tz,uni_xyz(:,m),uni_xyz(:,n))
            r=(tx-uni_xyz(1,n))**2
            r=r+(ty-uni_xyz(2,n))**2
            r=r+(tz-uni_xyz(3,n))**2
            if (r .gt. radial_min**2 .and. r .lt. radial_max**2) then
                iter=iter+1
            end if
        end do
    end do
    iter=iter*2

end subroutine radial_distribution
