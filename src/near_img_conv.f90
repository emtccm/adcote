subroutine near_img_conv(L,tx,ty,tz,uni_xyz_m,uni_xyz_n)
    implicit none

    real(kind=8) :: dx,dy,dz,L,tx,ty,tz
    real(kind=8),dimension(:) :: uni_xyz_m(3),uni_xyz_n(3)
   
    dx=uni_xyz_m(1)-uni_xyz_n(1)
    dy=uni_xyz_m(2)-uni_xyz_n(2)
    dz=uni_xyz_m(3)-uni_xyz_n(3)
    
    if (abs(dx) > 0.5*L) then
        if (dx < 0) then
            tx=uni_xyz_m(1)+L
        else
            tx=uni_xyz_m(1)-L
        end if
    else
        tx=uni_xyz_m(1)
    end if

    if (abs(dy) > 0.5*L) then
        if (dy < 0) then
            ty=uni_xyz_m(2)+L
        else
            ty=uni_xyz_m(2)-L
        end if
    else
        ty=uni_xyz_m(2)
    end if

    if (abs(dz) > 0.5*L) then
        if (dz < 0) then
            tz=uni_xyz_m(3)+L
        else
            tz=uni_xyz_m(3)-L
        end if
    else
        tz=uni_xyz_m(3)
    end if

end subroutine near_img_conv
