subroutine radial_results(dr,frag,L,Nmax,pop,rad_dist,radial_save,uout)
    implicit none

    integer :: i,frag,Nmax,uout
    integer, dimension(:) :: pop(frag)

    logical :: radial_save

    real(kind=8) :: dr,L
    real(kind=8), dimension(:) :: rad_dist(frag)

    write(unit = uout, fmt = '(1x,a14,1x,a28,1x,a14)') '**************','RADIAL DISTRIBUTION FUNCTION','**************'
    write(unit = uout, fmt = *) ''

    write(unit = uout, fmt = '(1x,a8,6x,a4)')  'Distance', 'g(r)'
    write(unit = uout, fmt = '(1x,a21)') '---------------------'
    do i = 1, frag, 1
        write(unit = uout, fmt = '(2x,f6.4,2x,f12.8)') rad_dist(i), (pop(i)*L**3)/(4*acos(-1.d0)*rad_dist(i)**2*dr*Nmax*Nmax)
    end do
    write(unit = uout, fmt = '(1x,a21)') '---------------------'
    write(unit = uout, fmt = *) ''

    if (radial_save .eqv. .true.) then
        open(unit = 15, file = 'rad_dist_func.out', action = 'write') 
        do i = 1, frag, 1
            write(unit = 15, fmt = '(2x,f6.4,2x,f12.8)') rad_dist(i), (pop(i)*L**3)/(4*acos(-1.d0)*rad_dist(i)**2*dr*Nmax*Nmax)
        end do
        close(unit = 15) 
    end if

end subroutine radial_results
