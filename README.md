# ADCOTE

Project for Computational Chemistry Programming Project

## Main features

- Easy to use clean inputs for Lennard-Jones potential simulation.
- Once compiled it can be used for many systems.
- Easy readable output.

## Installation instructions

1. Requirements
- gfortran 9.0.0 or higher
- zsh 5.8 or higher

2. Manual installation
```
cd path/to/adcote/scr
make
```

## Example

```
$comment
Calculation of 100 in 6x6x6 cube and print g(r) to file each 5 steps
$end

$general
particles       100
temperature     300
cube_len        6
steps           100000
$end

$advanced
threads         4
equilibration   10000
radial_save     true
radial_step     5
$end
```

## Contact info

Aitor Diaz 

ad.andres@outlook.com
